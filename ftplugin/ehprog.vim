" Vim filetype plugin
" Language:    ehprog
" Maintainer:  walterpi <waltersz@protonmail.com>
" Last Change: March 15, 2019
" URL:         https://gitlab.com/ehprog/vim-ehprog

" Only do this when not done yet for this buffer
if exists("b:did_ftplugin")
  finish
endif

" Don't load another plugin for this buffer
let b:did_ftplugin = 1

setl comments=:--:
setl commentstring=--%s
setl define=^\\s*let\\k*
" setl formatoptions+=croql
setl iskeyword=33,35,37,38,42,43,45,47-58,60-63,65-90,94,95,97-122,124
setl shiftwidth=4
setl tabstop=4

let b:undo_ftplugin = 'setlocal comments< commentstring< define< iskeyword< shiftwidth< tabstop<'
