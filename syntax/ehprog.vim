" Vim syntax plugin
" Language:    ehprog
" Maintainer:  walterpi <waltersz@protonmail.com>
" Last Change: March 15, 2019
" URL:         https://gitlab.com/ehprog/vim-ehprog

" For vim-version 5.x: Clear all syntax items
" For vim-version 6.x: Quit when a syntax file was already loaded
if version < 600
  syntax clear
elseif exists("b:current_syntax")
  finish
endif

syn match ehprogPath "\k\+\(\.\k+\)*\>"
syn keyword ehprogConst ^Nil
syn keyword ehprogBool True False ^True ^False
syn match ehprogNumber "\<[0-9]\+\(\.[0-9]\+\)?\>"
syn match ehprogNumber "\<[0-9]:[0-9]\+\>"
syn match ehprogNumber "\<[0-9]\/[0-9]\+\>"

syn match ehprogFunc "\<\(*\|+\|-\|/\|<\|<<\|<=\|=\|/=\|>\|>=\|>>\|**\|>>=\|:\|::\|<-\)\>"
syn match ehprogOperator "\<\(let\|fun\|rec\|define\|mod\|type\|dyn\|mut\|macro\|if\|when\|for\|while\|[a-z]\+:\)\>"
syn match ehprogType "\<\^\?[A-Z]\k*\>"

syn region ehprogString start=+"+ end=+"+ skip=+\\\\\|\\"+ contains=ehprogStringEscape,ehprogStringInterpolate
syn match ehprogStringEscape "\\[0nte"\\\~]" contained
syn region ehprogStringInterpolate matchgroup=PreProc start="\~(" end=")" contained fold

syn region ehprogCommentLine start="--" end="$" contains=ehprogTodo

syn keyword ehprogTodo contained TODO FIXME XXX NB NOTE

syn region ehprogLambda matchgroup=Delimiter start="\[" end="\]" transparent fold
syn region ehprogList matchgroup=Delimiter start="(" end=")" transparent fold
syn region ehprogQList matchgroup=PreProc start="`(" end=")" transparent fold
syn region ehprogIList matchgroup=PreProc start="\~(" end=")" transparent fold
syn region ehprogBind matchgroup=PreProc start="\$(" end=")" transparent fold

hi! def link ehprogConst Constant
hi! def link ehprogBool Boolean
hi! def link ehprogCommentLine Comment
hi! def link ehprogFunc Keyword
hi! def link ehprogType Type
hi! def link ehprogNumber Number
hi! def link ehprogOperator Keyword
hi! def link ehprogString String
hi! def link ehprogStringEscape Special
hi! def link ehprogTodo Todo

let b:current_syntax = 'ehprog'
